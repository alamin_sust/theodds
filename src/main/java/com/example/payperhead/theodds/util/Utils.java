package com.example.payperhead.theodds.util;

import java.util.Arrays;
import java.util.List;

/**
 * Created by md_al on 12-Jan-19.
 */
public class Utils {
    public static boolean isNullOrEmpty(String val) {
        return val==null || val.equals("");
    }

    public static boolean isValid(String val) {
        return !isNullOrEmpty(val);
    }

    public static List<String> getJuiceProfilesDropDowns() {
        return Arrays.asList("-105", "-108", "-110");
    }

    public static List<String> getBalanceTypesDropDowns() {
        return Arrays.asList("Zero Out Weekly", "Carry-Over");
    }

    public static List<String> getTeaserOptionsDropDowns() {
        return Arrays.asList("4 Pt. Basketball Teaser / 6 Pt. Football Teaser",
                "4½ Pt. Basketball Teaser / 6½ Pt. Football Teaser",
                "5 Pt. Basketball Teaser / 7 Pt. Football Teaser",
                "7 Pt. Basketball Teaser / 10 Pt. Football Teaser");
    }
}