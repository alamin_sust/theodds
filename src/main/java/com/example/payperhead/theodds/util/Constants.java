package com.example.payperhead.theodds.util;

/**
 * Created by md_al on 12-Jan-19.
 */
public class Constants {
    public static final String LOGOUT_SUCCESS = "Successfully Logged Out.";
    public static final String LOGIN_SUCCESS = "Successfully Logged In.";
    public static final String LOGIN_PASS_MISSMATCH = "Email and Password doesn't macth, please try again.";
    public static final String LOGIN_INVALID_USER_PASS = "Please enter valid email and password";

    public static final String REGISTER_USER_ALREADY_EXISTS = "User already exists with same username and email";
    public static final String ACCESS_DENIED = "Sorry, You don't have the access to take this action";
    public static final String REGISTER_SUCCESS = "Successfully registerd";
    public static final String ADD_SUCCESS = "Successfully added";
    public static final String EDIT_SUCCESS = "Successfully changed";
    public static final String DELETE_SUCCESS = "Successfully deleted";
    public static final String EMAIL_SUCCESS = "Email Sent!";

    public static final String CONTACT_EMAIL_ADDRESS = "alaminbbsc@gmail.com";
}
