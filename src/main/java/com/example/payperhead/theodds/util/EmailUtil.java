package com.example.payperhead.theodds.util;

import javax.mail.*;
import javax.mail.internet.*;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;

import static com.example.payperhead.theodds.util.Constants.CONTACT_EMAIL_ADDRESS;

/**
 * Created by md_al on 07-Mar-18.
 */
public class EmailUtil {
    public static void contactSendEmail(String name, String email, String address, String message) {
        String from = "alamincsesust";
        String pass = "69200811cseA!";
        String[] to = { CONTACT_EMAIL_ADDRESS }; // list of recipient email addresses
        String subject = "Feedback from "+name;
        String body = "Hi "+CONTACT_EMAIL_ADDRESS+", \n"+name+"(email: "+email+") from "+address+" Submitted a feedback. Here it is: \n" +
                " \n\n" +message;

        sendFromGMail(from, pass, to, subject, body, false);
    }

    private static void sendFromGMail(String from, String pass, String[] to, String subject, String body,boolean isHtml) {
        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(from));
            InternetAddress[] toAddress = new InternetAddress[to.length];

            // To get the array of addresses
            for( int i = 0; i < to.length; i++ ) {
                toAddress[i] = new InternetAddress(to[i]);
            }

            for( int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }

            message.setSubject(subject);


            if(isHtml) {
                message.setContent(body, "text/html" );
            } else {
                message.setText(body, "UTF-8", "html");
            }



            Transport transport = session.getTransport("smtp");
            transport.connect(host, from, pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        }
        catch (AddressException ae) {
            ae.printStackTrace();
        }
        catch (MessagingException me) {
            me.printStackTrace();
        }
    }
}