package com.example.payperhead.theodds.dao;

import com.example.payperhead.theodds.db.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by md_al on 12-Jan-19.
 */
@Repository
public interface AuthDao extends JpaRepository<User, Integer>{

    @Query("select u from User u where u.username=:username and u.password=:password")
    public List<User> findByUsernameAndPassword(@Param("username") String username,
                                          @Param("password") String password);

    @Query("select u from User u where u.username=:username and u.email=:email")
    public List<User> findByUsernameAndEmail(@Param("username") String username,
                                             @Param("email") String email);

    @Query("select u as player,ua as playerAgent from User u join PlayerAgent ua on ua.agentId=:id and ua.playerId = u.id")
    public List<Object> findAllPlayerByAgentId(@Param("id") int id);

    @Query("select u from User u where type='agent'")
    public List<User> findAllAgent();

    @Query("select u from User u where type='player'")
    public List<User> findAllPlayer();
}
