package com.example.payperhead.theodds.dao;

import com.example.payperhead.theodds.connection.Database;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by md_al on 12-Jan-19.
 */
@Repository
public class CrudDao {

    public void insertPlayerAgent(String agentId, Integer playerId) throws SQLException {
        Database db = new Database();
        db.connect();

        String q = "insert into player_agent (player_id, agent_id) " +
                " values("+playerId+","+agentId+")";
        Statement st = db.connection.createStatement();
        st.executeUpdate(q);
    }

    public String getNextTriningId(String userId, String parserId) throws SQLException {
        Database db = new Database();
        db.connect();

        try {
        String q = "select max(id)+1 mxId from training_file_set";

            Statement st = db.connection.createStatement();
            ResultSet rs = st.executeQuery(q);
            if (rs.next()) {
                return rs.getString("mxId")==null ? "1" : rs.getString("mxId");
            } else {
                return "1";
            }
        } catch (Exception e) {

        } finally {
            db.close();
        }

        return "1";
    }

    public String getNextParserId(String userId) throws SQLException {

        Database db = new Database();
        db.connect();

        try {
            String q = "select max(parser_id)+1 mxId from training_file_set where uploaded_by="
                    +userId;
            Statement st = db.connection.createStatement();
            ResultSet rs = st.executeQuery(q);
            if (rs.next()) {
                return rs.getString("mxId")==null?"1":rs.getString("mxId");
            } else {
                return "1";
            }
        } catch (Exception e) {

        } finally {
            db.close();
        }

        return "1";
    }

    public void insertIntoTrainingFileSetTable(String userId, String parserId, String nextId) throws SQLException {
        Database db = new Database();
        db.connect();

        String q = "insert into training_file_set (id, parser_id, uploaded_by, upload_date) " +
                " values("+nextId+","+parserId+","+userId+",now())";
        Statement st = db.connection.createStatement();
        st.executeUpdate(q);
    }
}
