package com.example.payperhead.theodds.dao;

import com.example.payperhead.theodds.db.Odds;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by md_al on 12-Jan-19.
 */
@Repository
public interface OddsDao extends JpaRepository<Odds, Integer>{

    @Query("select u from Odds u where agent_id=:id")
    List<Odds> getAllByAgentId(int id);
}
