package com.example.payperhead.theodds.service;

import com.example.payperhead.theodds.dao.AuthDao;
import com.example.payperhead.theodds.dao.CrudDao;
import com.example.payperhead.theodds.db.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.sql.SQLException;
import java.util.List;

import static com.example.payperhead.theodds.util.Constants.*;
import static com.example.payperhead.theodds.util.Utils.isValid;

/**
 * Created by md_al on 20-Jan-19.
 */
@Service
public class AuthService {

    @Autowired
    private HelperService helperService;

    @Autowired
    private AuthDao authDao;

    @Autowired
    private CrudDao crudDao;

    public void logout(HttpServletRequest request, HttpSession session) {
        if (request.getParameter("logout") != null) {
            helperService.updateSession(session, "successMessage", LOGOUT_SUCCESS);
        }
        helperService.updateSession(session, "id", null);
        helperService.updateSession(session, "username", null);
        helperService.updateSession(session, "userType", null);
    }

    public boolean login(HttpServletRequest request, HttpSession session) {

        String username = request.getParameter("username");
        String password = request.getParameter("password");

        if (isValid(username) && isValid(password)) {
            List<User> userList = authDao.findByUsernameAndPassword(username, password);
            if (userList.size() == 1) {
                User user = userList.get(0);
                helperService.updateSession(session, "id", String.valueOf(user.getId()));
                helperService.updateSession(session, "username", user.getUsername());
                helperService.updateSession(session, "userType", user.getType());
                helperService.updateSession(session, "successMessage", LOGIN_SUCCESS);
                return true;
            } else {
                helperService.updateSession(session, "errorMessage", LOGIN_PASS_MISSMATCH);
                return false;
            }
        } else {
            helperService.updateSession(session, "errorMessage", LOGIN_INVALID_USER_PASS);
            return false;
        }
    }

    public boolean register(HttpServletRequest request, HttpSession session) throws SQLException {

        String username = request.getParameter("username");
        String email  = request.getParameter("email");
        String userType  = request.getParameter("userType");

        if(!haveRegisterAccess(userType,session)) {
            helperService.updateSession(session, "errorMessage", ACCESS_DENIED);
            return false;
        }

        if (isValid(username) && isValid(email)) {
            List<User> userList = authDao.findByUsernameAndEmail(username, email);
            if (userList.size() == 0) {
                User user = new User();
                user.setUsername(username);
                user.setEmail(email);
                user.setPassword(request.getParameter("password"));

                if(userType.equals("agent")) {
                    user.setBalanceType(request.getParameter("balanceType"));
                    user.setJuiceProfile(request.getParameter("juiceProfile"));
                    user.setNumberOfPlayers(Integer.valueOf(request.getParameter("numberOfPlayers")));
                    user.setMaxTeamParlay(Integer.valueOf(request.getParameter("maxTeamParlay")));
                    user.setMaxBuyPoints(Integer.valueOf(request.getParameter("maxBuyPoints")));
                    user.setMaxTeamTeaser(Integer.valueOf(request.getParameter("maxTeamTeaser")));
                    user.setTeaserOption(request.getParameter("teaserOption"));
                }
                user.setType(request.getParameter("userType"));
                authDao.save(user);

                if(request.getParameter("userType").equals("player")) {
                    crudDao.insertPlayerAgent(session.getAttribute("id").toString(),user.getId());
                }

                helperService.updateSession(session, "successMessage", REGISTER_SUCCESS);

                if(userType.equals("agent")) {
                    helperService.updateSession(session, "id", String.valueOf(user.getId()));
                    helperService.updateSession(session, "username", user.getUsername());
                    helperService.updateSession(session, "userType", user.getType());
                }

                return true;
            } else {
                helperService.updateSession(session, "errorMessage", REGISTER_USER_ALREADY_EXISTS);
                return false;
            }
        }

        return false;
    }

    private boolean haveRegisterAccess(String userType, HttpSession session) {
        if (userType.equals("player") && (session.getAttribute("userType") == null || !session.getAttribute("userType").equals("agent"))) {
            return false;
        }
        return true;
    }

    public User getUserFromSession(HttpSession session) {
        return authDao.findById(session.getAttribute("id") == null?
                0:Integer.parseInt(session.getAttribute("id").toString()) ).orElse(null);
    }
}