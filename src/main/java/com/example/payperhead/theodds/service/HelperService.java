package com.example.payperhead.theodds.service;

import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.servlet.http.HttpSession;

/**
 * Created by md_al on 12-Jan-19.
 */
@Service
public class HelperService {
    public void updateSession(HttpSession session, String key, String value) {
        session.setAttribute(key, value);
    }

    public void populateModelWithSessionAttributes(Model model, HttpSession session) {

        model.addAttribute("successMessage", session.getAttribute("successMessage"));
        session.setAttribute("successMessage", null);

        model.addAttribute("errorMessage", session.getAttribute("errorMessage"));
        session.setAttribute("errorMessage", null);

        model.addAttribute("id", session.getAttribute("id"));
        model.addAttribute("email", session.getAttribute("email"));

    }
}
