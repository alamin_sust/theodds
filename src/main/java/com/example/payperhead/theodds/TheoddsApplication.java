package com.example.payperhead.theodds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TheoddsApplication {

	public static void main(String[] args) {
		SpringApplication.run(TheoddsApplication.class, args);
	}

}

