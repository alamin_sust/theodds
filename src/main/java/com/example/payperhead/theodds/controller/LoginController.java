package com.example.payperhead.theodds.controller;

import com.example.payperhead.theodds.service.AuthService;
import com.example.payperhead.theodds.service.HelperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by md_al on 20-Jan-19.
 */
@Controller
public class LoginController {

    @Autowired
    private AuthService authService;

    @Autowired
    private HelperService helperService;

    @RequestMapping("/login")
    public String login(Model model, HttpServletRequest request, HttpServletResponse response,
                        HttpSession session,
                        @RequestParam(name = "logout", defaultValue = "false") boolean logout) {
        if(logout) {
            authService.logout(request, session);
        }
        //helperService.populateModelWithSessionAttributes(model, session);
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST, params = "type")
    public String loginPost(Model model, HttpServletRequest request, HttpServletResponse response,
                            HttpSession session,
                            @RequestParam("type") String type) throws IOException, SQLException {
        if(type.equals("login") && authService.login(request, session)) {
            redirectTo(session, response);
        }
        if(type.equals("register") && authService.register(request, session)) {
            redirectTo(session, response);
        }

        return "login";
    }

    private void redirectTo(HttpSession session, HttpServletResponse response) throws IOException {
        String username = session.getAttribute("username").toString();
        String userType = session.getAttribute("userType").toString();
        if(userType.equals("agent") && username.equals("admin")) {
            response.sendRedirect("adminHome");
        } else if(userType.equals("agent")) {
            response.sendRedirect("agentHome");
        } else {
            response.sendRedirect("home");
        }
    }
}
