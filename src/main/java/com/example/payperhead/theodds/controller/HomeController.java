package com.example.payperhead.theodds.controller;

import com.example.payperhead.theodds.dao.AuthDao;
import com.example.payperhead.theodds.dao.OddsDao;
import com.example.payperhead.theodds.db.Odds;
import com.example.payperhead.theodds.db.PlayerAgent;
import com.example.payperhead.theodds.db.User;
import com.example.payperhead.theodds.service.AuthService;
import com.example.payperhead.theodds.service.HelperService;
import com.example.payperhead.theodds.util.EmailUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static com.example.payperhead.theodds.util.Constants.*;

/**
 * Created by md_al on 20-Jan-19.
 */
@Controller
public class HomeController {

    @Autowired
    private AuthService authService;

    @Autowired
    private HelperService helperService;

    @Autowired
    private AuthDao authDao;

    @Autowired
    private OddsDao oddsDao;

    @RequestMapping("/")
    public String root(Model model) {
        return "home";
    }

    @RequestMapping("/home")
    public String home(Model model, HttpSession session) {
        return "home";
    }

    @RequestMapping("/agentHome")
    public String agentHome(Model model, HttpSession session) {

        List<Object> objectList = (ArrayList) authDao.findAllPlayerByAgentId(Integer.parseInt(session.getAttribute("id").toString()));
        List<User> playerList = new LinkedList<>();
        int activePlayers = 0;
        double tolalBalance = 0;
        int totalPlayers = objectList.size();
        for (int i=0;i<objectList.size();i++) {
            Object object[] = (Object[]) objectList.get(i);
            User user = ((User)object[0]);
            user.setPlayerAgent((PlayerAgent) object[1]);
            if(user.getPlayerAgent().getStatus().equals("Active")) {
                activePlayers++;
            }
            tolalBalance+=Double.parseDouble(user.getPlayerAgent().getBalance());
            playerList.add(user);
        }

        model.addAttribute("playerList", playerList);
        model.addAttribute("activePlayers", Integer.toString(activePlayers));
        model.addAttribute("totalBalance", Double.toString(tolalBalance));
        model.addAttribute("chargeAmount", totalPlayers*5);
        model.addAttribute("totalPlayers", Integer.toString(totalPlayers));
        return "agentHome";
    }

    @RequestMapping(value = "/agentHome", method = RequestMethod.POST)
    public String agentHomePost(Model model, HttpSession session,
                                @RequestParam(name = "editPlayer", defaultValue = "-1") int editPlayer,
                                @RequestParam(name = "deletePlayer", defaultValue = "-1") int deletePlayer,
                                @RequestParam(name = "username", required = false) String username,
                                @RequestParam(name = "email", required = false) String email,
                                @RequestParam(name = "password", required = false) String password,
                                @RequestParam(name = "wagerLimit", required = false) String wagerLimit) {

        if(editPlayer >-1) {
            User user = authDao.getOne(editPlayer);
            user.setUsername(username);
            user.setEmail(email);
            user.setPassword(password);
            user.setWagerLimit(wagerLimit);
            authDao.save(user);
            helperService.updateSession(session, "successMessage", EDIT_SUCCESS);
        }

        if (deletePlayer >-1) {
            authDao.deleteById(deletePlayer);
            helperService.updateSession(session, "successMessage", DELETE_SUCCESS);
        }

        return "redirect:/agentHome";
    }

    @RequestMapping("/odds")
    public String odds(Model model, HttpSession session) {
        model.addAttribute("oddsList", oddsDao.getAllByAgentId(Integer.parseInt(session.getAttribute("id").toString())));
        return "odds";
    }

    @RequestMapping(value = "/odds", method = RequestMethod.POST)
    public String oddsPost(Model model, HttpSession session,
                                @RequestParam(name = "addOdds", defaultValue = "-1") int addOdds,
                                @RequestParam(name = "editOdds", defaultValue = "-1") int editOdds,
                                @RequestParam(name = "deleteOdds", defaultValue = "-1") int deleteOdds,
                                @RequestParam(name = "details", required = false) String details,
                                @RequestParam(name = "val", required = false) String val,
                                @RequestParam(name = "sports", required = false) String sports,
                                @RequestParam(name = "totalAmount", required = false) String totalAmount) {

        if (addOdds >-1) {
            Odds odds = new Odds();
            odds.setAgentId(Integer.valueOf(session.getAttribute("id").toString()));
            odds.setDetails(details);
            odds.setVal(val);
            odds.setSports(sports);
            odds.setTotalAmount(totalAmount);
            oddsDao.save(odds);
            helperService.updateSession(session, "successMessage", ADD_SUCCESS);
        }

        if(editOdds >-1) {
            Odds odds = oddsDao.getOne(editOdds);
            odds.setDetails(details);
            odds.setVal(val);
            odds.setSports(sports);
            odds.setTotalAmount(totalAmount);
            oddsDao.save(odds);
            helperService.updateSession(session, "successMessage", EDIT_SUCCESS);
        }

        if (deleteOdds >-1) {
            oddsDao.deleteById(deleteOdds);
            helperService.updateSession(session, "successMessage", DELETE_SUCCESS);
        }

        return "redirect:/odds";
    }

    @RequestMapping("/adminHome")
    public String adminHome(Model model, HttpSession session) {
        List<User> agentList = authDao.findAllAgent();
        for(int j=0;j<agentList.size();j++) {
            List<Object> objectList = (ArrayList) authDao.findAllPlayerByAgentId(agentList.get(j).getId());
            List<User> playerList = new LinkedList<>();
            int activePlayers = 0;
            double tolalBalance = 0;
            int totalPlayers = objectList.size();
            for (int i=0;i<objectList.size();i++) {
                Object object[] = (Object[]) objectList.get(i);
                User user = ((User)object[0]);
                user.setPlayerAgent((PlayerAgent) object[1]);
                if(user.getPlayerAgent().getStatus().equals("Active")) {
                    activePlayers++;
                }
                tolalBalance+=Double.parseDouble(user.getPlayerAgent().getBalance());
                playerList.add(user);
            }
            agentList.get(j).setPlayerList(playerList);
            agentList.get(j).setActivePlayers(activePlayers);
        }
        model.addAttribute("agentList", agentList);

        model.addAttribute("playerList", authDao.findAllPlayer());
        return "adminHome";
    }

    @RequestMapping("/blog")
    public String blog(Model model) {
        return "blog";
    }

    @RequestMapping("/about")
    public String about(Model model) {
        return "about";
    }

    @RequestMapping("/contact")
    public String contact(Model model) {
        return "contact";
    }

    @RequestMapping(value = "/contact", method = RequestMethod.POST)
    public String contactPost(Model model, HttpSession session,
                              @RequestParam(name = "name", required = false) String name,
                              @RequestParam(name = "email", required = false) String email,
                              @RequestParam(name = "address", required = false) String address,
                              @RequestParam(name = "message", required = false) String message) {

        EmailUtil.contactSendEmail(name,email,address,message);
        helperService.updateSession(session, "successMessage", EMAIL_SUCCESS);
        return "redirect:/contact";
    }

    @RequestMapping("/news")
    public String news(Model model) {
        return "news";
    }

    @RequestMapping("/team")
    public String team(Model model) {
        return "team";
    }

    @RequestMapping("/single-blog")
    public String singleblog(Model model) {
        return "single-blog";
    }
}
