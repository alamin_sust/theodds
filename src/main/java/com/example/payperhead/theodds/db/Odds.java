package com.example.payperhead.theodds.db;

import javax.persistence.*;

@Entity
@Table(name = "odds")
public class Odds {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    Integer id;

    @Column(name = "val")
    String val;

    @Column(name = "sports")
    String sports;

    @Column(name = "details")
    String details;

    @Column(name = "agent_id")
    Integer agentId;

    @Column(name = "player_id")
    String playerId;

    @Column(name = "total_amount")
    String totalAmount;

    public Odds(String val, String sports, String details, Integer agentId, String playerId, String totalAmount) {
        this.val = val;
        this.sports = sports;
        this.details = details;
        this.agentId = agentId;
        this.playerId = playerId;
        this.totalAmount = totalAmount;
    }

    public Odds() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public String getSports() {
        return sports;
    }

    public void setSports(String sports) {
        this.sports = sports;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }
}
