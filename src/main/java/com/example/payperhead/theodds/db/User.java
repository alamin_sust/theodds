package com.example.payperhead.theodds.db;

import javax.jws.soap.SOAPBinding;
import javax.persistence.*;
import java.util.List;

/**
 * Created by md_al on 12-Jan-19.
 */
@Entity
@Table(name = "user")
public class User {

    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    Integer id;

    @Column(name = "username")
    String username;

    @Column(name = "email")
    String email;

    @Column(name = "password")
    String password;

    @Column(name = "balance_type")
    String balanceType;

    @Column(name = "juice_profile")
    String juiceProfile;

    @Column(name = "number_of_players")
    Integer numberOfPlayers;

    @Column(name = "max_team_parlay")
    Integer maxTeamParlay;

    @Column(name = "max_buy_points")
    Integer maxBuyPoints;

    @Column(name = "max_team_teaser")
    Integer maxTeamTeaser;

    @Column(name = "teaser_option")
    String teaserOption;

    @Column(name = "type")
    String type;

    @Column(name = "wager_limit")
    String wagerLimit;

    @Transient
    PlayerAgent playerAgent;

    @Transient
    List<User> playerList;

    @Transient
    Integer activePlayers;

    public User() {

    }


    public User(String username, String email, String password, String balanceType, String juiceProfile, Integer numberOfPlayers, Integer maxTeamParlay, Integer maxBuyPoints, Integer maxTeamTeaser, String teaserOption, String type, String wagerLimit) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.balanceType = balanceType;
        this.juiceProfile = juiceProfile;
        this.numberOfPlayers = numberOfPlayers;
        this.maxTeamParlay = maxTeamParlay;
        this.maxBuyPoints = maxBuyPoints;
        this.maxTeamTeaser = maxTeamTeaser;
        this.teaserOption = teaserOption;
        this.type = type;
        this.wagerLimit = wagerLimit;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBalanceType() {
        return balanceType;
    }

    public void setBalanceType(String balanceType) {
        this.balanceType = balanceType;
    }

    public String getJuiceProfile() {
        return juiceProfile;
    }

    public void setJuiceProfile(String juiceProfile) {
        this.juiceProfile = juiceProfile;
    }

    public Integer getNumberOfPlayers() {
        return numberOfPlayers;
    }

    public void setNumberOfPlayers(Integer numberOfPlayers) {
        this.numberOfPlayers = numberOfPlayers;
    }

    public Integer getMaxTeamParlay() {
        return maxTeamParlay;
    }

    public void setMaxTeamParlay(Integer maxTeamParlay) {
        this.maxTeamParlay = maxTeamParlay;
    }

    public Integer getMaxBuyPoints() {
        return maxBuyPoints;
    }

    public void setMaxBuyPoints(Integer maxBuyPoints) {
        this.maxBuyPoints = maxBuyPoints;
    }

    public Integer getMaxTeamTeaser() {
        return maxTeamTeaser;
    }

    public void setMaxTeamTeaser(Integer maxTeamTeaser) {
        this.maxTeamTeaser = maxTeamTeaser;
    }

    public String getTeaserOption() {
        return teaserOption;
    }

    public void setTeaserOption(String teaserOption) {
        this.teaserOption = teaserOption;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public PlayerAgent getPlayerAgent() {
        return playerAgent;
    }

    public void setPlayerAgent(PlayerAgent playerAgent) {
        this.playerAgent = playerAgent;
    }

    public List<User> getPlayerList() {
        return playerList;
    }

    public void setPlayerList(List<User> playerList) {
        this.playerList = playerList;
    }

    public Integer getActivePlayers() {
        return activePlayers;
    }

    public void setActivePlayers(Integer activePlayers) {
        this.activePlayers = activePlayers;
    }

    public String getWagerLimit() {
        return wagerLimit;
    }

    public void setWagerLimit(String wagerLimit) {
        this.wagerLimit = wagerLimit;
    }
}
