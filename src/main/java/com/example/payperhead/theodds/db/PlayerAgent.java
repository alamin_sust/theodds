package com.example.payperhead.theodds.db;

import javax.persistence.*;

/**
 * Created by md_al on 26-Jan-19.
 */
@Entity
@Table(name = "player_agent")
public class PlayerAgent {
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    Integer id;

    @Column(name = "player_id")
    Integer playerId;

    @Column(name = "agent_id")
    Integer agentId;

    @Column(name = "balance")
    String balance;

    @Column(name = "status")
    String status;

    public PlayerAgent() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
