-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: theodds
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `odds`
--

DROP TABLE IF EXISTS `odds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `odds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `val` varchar(110) DEFAULT NULL,
  `sports` varchar(110) DEFAULT NULL,
  `details` varchar(1010) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `player_id` int(11) DEFAULT NULL,
  `total_amount` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `odds_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `odds`
--

LOCK TABLES `odds` WRITE;
/*!40000 ALTER TABLE `odds` DISABLE KEYS */;
INSERT INTO `odds` VALUES (2,'56.99','Footballpppp','yrtypppp',8,NULL,'5.998888'),(3,'dfghd','dfghd','hdfgh',8,NULL,'dfghd'),(5,'cvbxhhh','cbhhh','bc nxhh',20,NULL,'cvbhhh');
/*!40000 ALTER TABLE `odds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player_agent`
--

DROP TABLE IF EXISTS `player_agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player_agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) NOT NULL,
  `agent_id` int(11) NOT NULL,
  `balance` varchar(110) DEFAULT '0',
  `status` varchar(110) DEFAULT 'Active',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_agent_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player_agent`
--

LOCK TABLES `player_agent` WRITE;
/*!40000 ALTER TABLE `player_agent` DISABLE KEYS */;
INSERT INTO `player_agent` VALUES (1,2,1,'10.5','Active'),(2,3,1,'99.99','Incactive'),(3,4,5,'0.2','Active'),(4,17,8,'0','Active'),(5,18,8,'0','Active'),(6,19,8,'0','Active'),(7,21,20,'0','Active'),(8,22,20,'0','Active'),(9,23,5,'0','Active');
/*!40000 ALTER TABLE `player_agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(110) NOT NULL,
  `email` varchar(110) NOT NULL,
  `password` varchar(110) NOT NULL,
  `balance_type` varchar(110) DEFAULT NULL,
  `juice_profile` varchar(110) DEFAULT NULL,
  `number_of_players` int(11) DEFAULT NULL,
  `max_team_parlay` int(11) DEFAULT NULL,
  `max_buy_points` int(11) DEFAULT NULL,
  `max_team_teaser` int(11) DEFAULT NULL,
  `teaser_option` varchar(110) DEFAULT NULL,
  `type` varchar(110) DEFAULT NULL,
  `wager_limit` varchar(110) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `agent_id_uindex` (`id`),
  UNIQUE KEY `agent_username_uindex` (`username`),
  UNIQUE KEY `agent_email_uindex` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'aa','aa@dd','11','Zero Out Weekly','- 105',6,6,6545,6,'5 Pt. Basketball Teaser / 7 Pt. Football Teaser','agent','0'),(3,'uu2','uu2@uu.com','11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'player','0'),(5,'admin','admin@theodds.com','11','Zero Out Weekly','- 105',1,3,1,3,'4 Pt. Basketball Teaser / 6 Pt. Football Teaser','agent','0'),(6,'aaa','aaa@aaa','11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'player','0'),(7,'aab','aaaa@sss','11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'player','0'),(8,'agent','agent@agent','11','Zero Out Weekly','- 108',4,5,3,5,'4Â½ Pt. Basketball Teaser / 6Â½ Pt. Football Teaser','agent','0'),(9,'player','player@player','11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'player','0'),(10,'p2','p2@p2','11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'player','0'),(11,'a2','a2@a2','11','Zero Out Weekly','- 108',6,5,12,4,'4 Pt. Basketball Teaser / 6 Pt. Football Teaser','agent','0'),(12,'p3','p3@p3','11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'player','0'),(13,'p4','p4@p4','11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'player','0'),(14,'p5','p5@p5','11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'player','0'),(15,'p6','p6@p6','11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'player','0'),(16,'p7','p7@p7','11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'player','0'),(18,'p10','p10@p10','11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'player','0'),(19,'pp','pp@pp','11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'player','0'),(20,'ta','ta@ta','11','Zero Out Weekly','- 108',3,4,32,4,'5 Pt. Basketball Teaser / 7 Pt. Football Teaser','agent',NULL),(22,'tp2','tp2@tp2','11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'player',NULL),(23,'ppppp','pp@ter','11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'player',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-06  5:36:29
