-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: theodds
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `player_agent`
--

DROP TABLE IF EXISTS `player_agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player_agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) NOT NULL,
  `agent_id` int(11) NOT NULL,
  `balance` varchar(110) DEFAULT NULL,
  `status` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_agent_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player_agent`
--

LOCK TABLES `player_agent` WRITE;
/*!40000 ALTER TABLE `player_agent` DISABLE KEYS */;
INSERT INTO `player_agent` VALUES (1,2,1,'10.5','Active'),(2,3,1,'99.99','Incactive'),(3,4,5,'0.2','Active');
/*!40000 ALTER TABLE `player_agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(110) NOT NULL,
  `email` varchar(110) NOT NULL,
  `password` varchar(110) NOT NULL,
  `balance_type` varchar(110) DEFAULT NULL,
  `juice_profile` varchar(110) DEFAULT NULL,
  `number_of_players` int(11) DEFAULT NULL,
  `max_team_parlay` int(11) DEFAULT NULL,
  `max_buy_points` int(11) DEFAULT NULL,
  `max_team_teaser` int(11) DEFAULT NULL,
  `teaser_option` varchar(110) DEFAULT NULL,
  `type` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `agent_id_uindex` (`id`),
  UNIQUE KEY `agent_username_uindex` (`username`),
  UNIQUE KEY `agent_email_uindex` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'aa','aa@dd','11','Zero Out Weekly','- 105',6,6,6545,6,'5 Pt. Basketball Teaser / 7 Pt. Football Teaser','agent'),(2,'uu','uu@uu.com','11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'player'),(3,'uu2','uu2@uu.com','11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'player'),(4,'uu3','uu3@uu.com','11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'player'),(5,'admin','admin@theodds.com','11','Zero Out Weekly','- 105',1,3,1,3,'4 Pt. Basketball Teaser / 6 Pt. Football Teaser','agent'),(6,'aaa','aaa@aaa','11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'player'),(7,'aab','aaaa@sss','11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'player');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-27  1:50:17
