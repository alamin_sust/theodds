<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
	<meta charset="utf-8">
	<title>Form-v10 by Colorlib</title>
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Font-->
	<link rel="stylesheet" type="text/css" href="css/montserrat-font.css">
	<link rel="stylesheet" type="text/css" href="fonts/material-design-iconic-font/css/material-design-iconic-font.min.css">
	<!-- Main Style Css -->
    <link rel="stylesheet" href="css/style.css"/>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body class="form-v10">

	<div class="page-content">
		<div class="form-v10-content">
			<%if(session.getAttribute("successMessage") != null){%>
			<div class="alert alert-success"><%=session.getAttribute("successMessage")%></div>
			<%}
				session.setAttribute("successMessage", null);%>
			<%if(session.getAttribute("errorMessage") != null){%>
			<div class="alert alert-danger"><%=session.getAttribute("errorMessage")%></div>
			<%}
			session.setAttribute("errorMessage", null);%>
			<form class="form-detail" action="login" method="post" id="myform">
				<input type="hidden" name="userType" value="<%=request.getParameter("userType")%>"/>
				<input type="hidden" name="type" value="<%=request.getParameter("type")%>"/>

				<%if(request.getParameter("type").equals("login")){%>
					<div class="form-right">
						<h2>
							<%=request.getParameter("userType").substring(0,1).toUpperCase()+request.getParameter("userType").substring(1)%>
							Login
						</h2>
						<div class="form-row">
							<input type="text" name="username" placeholder="Username" required>
						</div>
						<div class="form-row">
							<input type="password" name="password" placeholder="Password" required>
						</div>

						<div class="form-row-last">
							<input type="submit" name="login" class="register" value="login">
						</div>
						<div class="form-row-last">
							<input type="button" onclick="window.location.href='home'" value="Back to Homepage" class="register"/>
						</div>
					</div>
				<%} else {%>
				<%if(request.getParameter("userType").equals("agent")){%>
					<div class="form-left">
						<h2>Agent Registration</h2>
						<h2>General Infomation</h2>
						<div class="form-row">
							<input type="text" name="username" placeholder="Username" required>
						</div>
						<div class="form-row">
							<input type="text" name="email" placeholder="Email" required>
						</div>
						<div class="form-row">
							<input type="password" name="password" placeholder="Password" required>
						</div>
					</div>
					<div class="form-right">
						<h2>Account Details</h2>

						<div class="form-row">
							<select name="numberOfPlayers" required>
								<option class="option" value="">-- Number of Players --</option>
								<c:forEach begin="1" end="19" step="1" var="loop">
									<option class="option" value="${loop}">${loop} Player(s)</option>
								</c:forEach>
							</select>
							<span class="select-btn">
								<i class="zmdi zmdi-chevron-down"></i>
							</span>
						</div>

						<div class="form-row">
							<select name="juiceProfile" required>
								<option class="option" value="">-- Juice Profile --</option>
								<option class="option" value="- 105">- 105</option>
								<option class="option" value="- 108">- 108</option>
								<option class="option" value="- 110">- 110</option>
							</select>
							<span class="select-btn">
								<i class="zmdi zmdi-chevron-down"></i>
							</span>
						</div>

						<div class="form-row">
							<select name="balanceType">
								<option class="option" value="">-- Balance Type --</option>
								<option class="option" value="Zero Out Weekly">Zero Out Weekly</option>
								<option class="option" value="Carry-Over">Carry-Over</option>
							</select>
							<span class="select-btn">
								<i class="zmdi zmdi-chevron-down"></i>
							</span>
						</div>

						<div class="form-row">
							<select name="maxTeamParlay" required>
								<option class="option" value="">-- Max Team Parlay --</option>
								<c:forEach begin="3" end="8" step="1" var="loop">
									<option class="option" value="${loop}">${loop} Team(s)</option>
								</c:forEach>
							</select>
							<span class="select-btn">
								<i class="zmdi zmdi-chevron-down"></i>
							</span>
						</div>

						<div class="form-row">
							<input type="number" name="maxBuyPoints" placeholder="Max Buy Points" required>
						</div>

						<div class="form-row">
							<select name="maxTeamTeaser" required>
								<option class="option" value="">-- Max Team Teaser --</option>
								<c:forEach begin="3" end="8" step="1" var="loop">
									<option class="option" value="${loop}">${loop} Team(s)</option>
								</c:forEach>
							</select>
							<span class="select-btn">
								<i class="zmdi zmdi-chevron-down"></i>
							</span>
						</div>

						<div class="form-row">
							<select name="teaserOption" required>
								<option class="option" value="">-- Teaser Option --</option>
								<option class="option" value="4 Pt. Basketball Teaser / 6 Pt. Football Teaser">4 Pt. Basketball Teaser / 6 Pt. Football Teaser</option>
								<option class="option" value="4½ Pt. Basketball Teaser / 6½ Pt. Football Teaser">4.5 Pt. Basketball Teaser / 6.5 Pt. Football Teaser</option>
								<option class="option" value="5 Pt. Basketball Teaser / 7 Pt. Football Teaser">5 Pt. Basketball Teaser / 7 Pt. Football Teaser</option>
								<option class="option" value="7 Pt. Basketball Teaser / 10 Pt. Football Teaser">7 Pt. Basketball Teaser / 10 Pt. Football Teaser</option>
							</select>
							<span class="select-btn">
								<i class="zmdi zmdi-chevron-down"></i>
							</span>
						</div>

						<%--<div class="form-checkbox">
							<label class="container"><p>I do accept the <a href="#" class="text">Terms and Conditions</a> of your site.</p>
								<input type="checkbox" name="checkbox">
								<span class="checkmark"></span>
							</label>
						</div>--%>
						<div class="form-row-last">
							<input type="submit" name="register" class="register" value="Register">
						</div>
						<div class="form-row-last">
							<input type="button" onclick="window.location.href='home'" value="Back to Homepage" class="register"/>
						</div>
					</div>
				<%} else if (request.getParameter("userType").equals("player")){%>
				<div class="form-right">
					<h2>Player Registration</h2>
					<div class="form-row">
						<input type="text" name="username" placeholder="Username" required>
					</div>
					<div class="form-row">
						<input type="text" name="email" placeholder="Email" required>
					</div>
					<div class="form-row">
						<input type="password" name="password" placeholder="Password" required>
					</div>

					<div class="form-row-last">
						<input type="submit" name="register" class="register" value="Register">
					</div>
					<div class="form-row-last">
						<input type="button" onclick="window.location.href='home'" value="Back to Homepage" class="register"/>
					</div>
				</div>
				<%}%>
				<%}%>

			</form>
		</div>
	</div>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>